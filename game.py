import random

class Game:
    def __init__(self, items, items_russian):
        self.items = items
        self.items_russian = items_russian
        self.run = True

    def play_english(self):
        print(10 * '*')
        while self.run:
            opponent_choice = random.choice(self.items)
            user_choice = input('Choose one thing Rock, Paper or Scissors (type quit to exit the game) ').lower()

            if user_choice == 'rock' and opponent_choice == 'scissors':
                print(10 * '*')
                print(user_choice)
                print(opponent_choice)
                print('Congrats!!! You Won!')
                print(10 * '*')

            elif user_choice == 'quit':
                self.run = False
                print(10 * '*')
            
            elif user_choice == opponent_choice:
                print(10 * '*')
                print(user_choice)
                print(opponent_choice)
                print('TIE')
                print(10 * '*')
            
            elif user_choice == 'paper' and opponent_choice == 'rock':
                print(10 * '*')
                print(user_choice)
                print(opponent_choice)
                print('Congrats!!! You Won!')
                print(10 * '*')
            
            elif user_choice == 'scissors' and opponent_choice == 'paper':
                print(10 * '*')
                print(user_choice)
                print(opponent_choice)
                print('Congrats!!! You Won!')
                print(10 * '*')

            else:
                print(10 * '*')
                print(user_choice)
                print(opponent_choice)
                print('I\'m Sorry But You Losed!!!')
                print(10 * '*')
        
        
    def play_russian(self):
        print(10 * '*')
        while self.run:
            opponent_choice = random.choice(self.items_russian)
            user_choice = input('Что выбираете Камень Ножницы или Бумага (введите quit чтобы покинуть игру) ').lower()

            if user_choice == 'камень' and opponent_choice == 'ножницы':
                print(10 * '*')
                print(user_choice)
                print(opponent_choice)
                print('Поздравляю!!! Вы выиграли!')
                print(10 * '*')

            elif user_choice == 'quit':
                self.run = False
                print(10 * '*')
            
            elif user_choice == opponent_choice:
                print(10 * '*')
                print(user_choice)
                print(opponent_choice)
                print('НИЧЬЯ')
                print(10 * '*')
            
            elif user_choice == 'бумага' and opponent_choice == 'камень':
                print(10 * '*')
                print(user_choice)
                print(opponent_choice)
                print('Поздравляю!!! Вы выиграли!')
                print(10 * '*')
            
            elif user_choice == 'ножницы' and opponent_choice == 'бумага':
                print(10 * '*')
                print(user_choice)
                print(opponent_choice)
                print('Поздравляю!!! Вы выиграли!')
                print(10 * '*')

            else:
                print(10 * '*')
                print(user_choice)
                print(opponent_choice)
                print('Мне очень жаль но вы проиграли!!!')
                print(10 * '*')



items = ['rock', 'paper', 'scissors']
items_russian = ['камень', 'ножницы', 'бумага']
game = Game(items, items_russian)
choice = input('English or Russian: ').lower()
if choice == 'english':
    game.play_english()
elif choice == 'russian':
    game.play_russian()
else:
    print("Enter 'English or Russian' to play")
    
